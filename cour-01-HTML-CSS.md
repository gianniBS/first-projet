 # COUR DE HTML/CSS

 - [Les bases du HTML](#les-bases-du-html)
 - Les bases du CSS (Partie 1)
 - Les bases du CSS (Partie 2)

 ### Les bases du HTML
[HTML](#cour-de-htmlcss)

    - le HTML c'est ce qui est visible sur un site
    - crée un fichier se terminant par .HTML
    - il est recommender de mettre la langue du site pour le référencement (c'est ça position dans les moteur de recherche)
    - il y a la body (le corps du site) ou se trouve trois partie: le header, la div et le footer
    - le header est la "tête" de page
    - la div est le "corps" de la page
    - le footer est le "bas" de page, c'est l'endroit ou on met généralement les liens, les certifications, les copyrights, les formulaires, etc...
    - les trois sont considéré comme des balises et sur c'est balises il y en a toujours deux, une ouvrant et une fermant sauf la balise "img"(image)
    - toute les balises se commence par un chevron ouvert et se finisse par un chevron fermé
    - dans les HTML le "p" signifie text/paragraphe qui peux se positionné n'importe ou dans le corps du site
    - il en excite plein d'autres mais dans les bases on utilise ("p", "b", "h1" et ça peut aller jusqu'à "h5", "a")
    - "a" sert à mettre un mot/phrase qui redirige vers un autres lien
    - il a les liens qui est très utile pour la navigation, les boutons, les PDF, réseau sociaux/autres sites mais également pour faire en sort que ca nous redirige vers une partie inférieur de la page
    - il y a "img" qui permet de mettre des image sur votre page, "img" est une balise qui se suffit à ell même qui veut dire qu'elle n'a pas de balise fermant
    - strong permet de mettre soit la phrase ou même certains mots de la phrase en gras
    - on peut utilisé "ul" pour faire une liste comme on peut utilisé "ol" pour faire une liste numérotée
    - à ne pas oublié que dans "ul" ou même "ol" il faut utilisé "li" pour ensuite mettre du texte et refermé par "li"