<font color='green'>Ne mets pas tout tes textes dans des blocs comme ça, il nn'y a plus d'intérêt au MD sinon. Et n'hésites pas à mettre des exemples.</font>

# COUR 00

## INTRODUCTION

 - [Création de projet gitlab](#cration-de-projet-gitlab)
 - [Projet gitlab sur webstorm](#projet-gitlab-sur-webstorm)
 - [Céation de branches sur gitlab et webstorm](#cation-de-branches-sur-gitlab-et-webstorm)
 - [En cas de conflit dans la merge](#en-cas-de-conflit-dans-la-merge)
 - [Prise de notes en Markdown](#prise-de-notes-en-markdown)

- ### Création de projet gitlab
[INTRODUCTION](#introduction)

    
    - crée projet
    - Information sur le projet ( nom, projet slug, description, visibiliter du projet et si on veut que le projet personnel ou dans un groupe )

---

- ### Projet gitlab sur webstorm
[INTRODUCTION](#introduction)
  

    - cloner le lien en HTTPS qui se trouve sur gitlab
    - on ouvre webstorm
    - ensuite on click sur le bouton "get from VSC"
    - puis aller sur copier l'url et copier le clone qui a été fait précédemment

---

- ### Céation de branches sur gitlab et webstorm
[INTRODUCTION](#introduction)


    (pour la création du projet il est grandement déconseillé d'écrire dans la master)
    - aller sur l'onglet master
    - sélectionné "new branch" puis écriver votre nom de branche ensuite appuié sur create (ou si vous en avez dêjà crée aller sur la branche crée/aller sur recherche si vous avez plusieurs
    (que ça soit pour un nouveau fichier ou pour une nouvelle branche les spaces ne fonction pas)
    - on peut modifier le texte où rajoutée du contenu dans cette nouvelle branche
    - aprés avoir rajouter du contenu sur la branche on va faire un "commit" (enregistré les modification) pour ensuite "push" (envoyer le contenu/modification de la branche sur le gitlab)
    - aprés ça aller sur gitlab
    - pour fusionner la branche avec le master aller dans create merge requests puis sur submit merge requests
    - que si vous êtes le chef du projet vous pouvez acceptée le modification faite avec le merge requests pour ça vous n'aurez qu'a appuyer sur merge
    - et votre branche sera incorporer dans votre master
    - vous pourez ensuite regardé les changement fait dans l'onglet de project dans "changes"
    - aprés tous ça retourné dans votre webstorm aller au niveau des branches et sélectionné la branche master puis clicker sur checkout puis appuyer sur la flèche bleu en haut de l'écran pour update project (CTRL + T)
    - vous pourez ensuite retourné au niveau des branches sélectionné votre branche clicker dessus et aller sur "delete" pour supprimé

---

 - ### En cas de conflit dans la merge
[INTRODUCTION](#introduction)

    (les conflit arrive souvent quand il y a plusieur personne sur le même projet et quand la branche n'est pas identique àla master sans compter les modifications)
   
---

 - ### Prise de notes en Markdown
[INTRODUCTION](#introduction)


    - dans les prise de note en markdown il faut obligatoirment être en fichier .md
    - pour les titre il faut utilisé des dièse et d'un space, il y a plusieurs niveau de titre plus on monte dans le niveau plus il faut rajoutée des dièse et mois on vois le titre
    # bonjour
    ## bonjour
    ### bonjour
    #### bonjour
    ##### bonjour
    ###### bonjour
    (PS: il faut toujours que la dièse soit niveau du debut de ligne)
    - on peut aussi mettre certains mot ou même des phrases en italique entre des étoiles ou des underscores (*)/(_)
    *bonjour*
    _bonjour_
    - il est aussi possible de mettre en gras en mettant le mot ou la phrase entre deux étoiles ou deux underscores de chaque côté (**)/(__)
    **bonjour**
    __bonjour__
    - il est aussi possible de mettre les deux en même temps (*__) (__*)/(_**) (**_)
    *__bonjour__*
    _**bonjour**_
    - il y a les sitation pour ça il va falloir utilisé des chevron en début de ligne (>)
    >bonjour
    - on peut mettre des lien que ça soit des lien internet ou même des lien qui ramène plus bas ou plus haut dans la documentation
    www.google.com
    - pour surligne on utilise des backtick
    `const bonjour = 4`
    - si vous voulez la la couleur sur certains mot il faudra utilisé trois backtick (mais attention ceci ne fonction que sur du code) et précise le type de code à utilisé
    ```javascript
    const bonjour = 4
    ```
    - ça permettra de prendre des note propre et facile a relire
